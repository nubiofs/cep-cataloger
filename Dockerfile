# Use official Maven's alpine image as base
FROM maven:3.6-jdk-13

#Environment Variables
ENV REDIS_HOSTNAME redis-cep-cataloger

RUN mkdir -p /cep-cataloger/
ADD . /cep-cataloger/
WORKDIR /cep-cataloger/

RUN mvn package

#Compilation

CMD "java -jar target/cep-cataloger-1.0.jar redis-cep-cataloger"
