import cephandler.helper.LocalPersistenceMappingHelper;
import cephandler.utils.InterscityEventType;
import com.fasterxml.uuid.Generators;
import helper.InterscityMessageHelper;
import org.rapidoid.annotation.Required;
import org.rapidoid.annotation.Run;
import org.rapidoid.setup.On;
import org.rapidoid.u.U;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.*;

@Run
public class Cataloger{
    private static final String REDIS_COMPOSER_HOST = System.getenv("REDIS_COMPOSER_HOST");

    public static void main (String[] args) {

        final String host;
        if(args.length == 0) host = System.getenv("REDIS_COMPOSER_HOST");
        else host = args[0];

        On.port(8090);

        On.get("/").json(()->U.map("message","You are connected to cep-cataloger"));

        On.get("/primitives_available/{uuid}").json((String uuid) -> getPrimitivesAvailable(uuid));

        On.get("/event_types").json(() -> getEventTypes(null,host));

        On.post("/event_types").json((@Required String name,
                                      @Required String definition,
                                      @Required String resourceUuid,
                                      @Required String capabilities,
                                      @Required String inputs) ->
                CreateEventType(name,definition, resourceUuid, capabilities,inputs,host));

        On.get("/event_types/{uuid}").json((String uuid) -> getTypes(uuid,host));


        On.get("/web_hooks").json(() -> getWebHooks(host));

        On.post("/web_hooks").json((@Required String uuid, @Required String url) -> registerWebHooks(uuid,url,host));

        On.delete("/web_hooks/{uuid}/{url}").json((String uuid, String url) -> deleteWebHook(uuid,url,host));


    }

    private static Map getPrimitivesAvailable(String rUuid) {
        LocalPersistenceMappingHelper  mappingHelper = new LocalPersistenceMappingHelper(REDIS_COMPOSER_HOST);

        Set<String> keys = mappingHelper.keysWith(rUuid);

        Map<String, Map<String, String>> eventsAvailable = new HashMap<>();
        try {
            Iterator<String> iterator = keys.iterator();
            while (iterator.hasNext()){
                String currentKey = iterator.next().substring(23);
                InterscityEventType eventType = mappingHelper.get(currentKey);
                Map<String, String> mData = new HashMap<>();
                mData.put("EventName",eventType.getEventName());
                mData.put("ResourceUuid",eventType.getResourceUuid());
                mData.put("Capability",eventType.getCapability());


                eventsAvailable.put(eventType.getCepEventUuid(),mData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            mappingHelper.close();
        }
        return U.map("response", eventsAvailable);
    }

    static Map CreateEventType(String Name, String Definition, String resourceUuid, String capabilities, String inputs, String host) {
        String[] Capabilities = StringToArray(capabilities);
        String[] Inputs = StringToArray(inputs);
        Jedis jedis = new Jedis(host);
        LocalPersistenceMappingHelper redisComposerMapping = new LocalPersistenceMappingHelper(REDIS_COMPOSER_HOST);

        for (int i = 0, j = 0; i < Capabilities.length ; i++){
//            UUID uuid = Generators.timeBasedGenerator().generate();
            try {
                InterscityEventType interscityEventType = InterscityMessageHelper.getInstance().
                        searchForInterscityEventType(resourceUuid,Capabilities[i]);
                if(interscityEventType != null) {
                    jedis.set(interscityEventType.getCepEventUuid() + ":Name", interscityEventType.getEventName());
                    jedis.set(interscityEventType.getCepEventUuid() + ":AvroSchema", interscityEventType.getAvroSchemaString());
                    jedis.sadd("Primitives", interscityEventType.getCepEventUuid());

                    /*counting++...*/
                    redisComposerMapping.incrEventCount(interscityEventType.getCepEventUuid());

                    Inputs[j++] = interscityEventType.getCepEventUuid();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        UUID uuid = Generators.timeBasedGenerator().generate();
        jedis.zadd("Unnasigned",System.currentTimeMillis(),uuid.toString());
        jedis.set(uuid.toString()+":Name",Name);
        jedis.set(uuid.toString()+":Definition",Definition);
        jedis.sadd(uuid.toString()+":Inputs", Inputs);
        jedis.close();
        redisComposerMapping.close();
        return U.map("msg","succesful register");
    }

    private static String GetCapabilityAvroSchema(String resourceUuid, String CapabilityName){
        InterscityEventType interscityEventType;
        try {
           interscityEventType = InterscityMessageHelper.getInstance().
                   searchForInterscityEventType(resourceUuid,CapabilityName);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return interscityEventType.getAvroSchemaString();
    }

    private static String [] StringToArray(String array){
        String [] result;
        result = array.split(",");
        if(result.length > 1) {
            result[0] = String.valueOf(result[0].charAt(1));
            result[result.length - 1] = String.valueOf(result[result.length - 1].charAt(1));
        }
        return result;
    }

    static Map getTypes(String uuid, String host){
        return U.map("response",getEventTypes(uuid,host));
    }


    private static Map[] getEventTypes(String uuid,String host) {

        Jedis jedis = new Jedis(host);
        HashMap<String, String>[] TypeMetaData = new HashMap[Math.toIntExact(jedis.scard("Registered"))];
        if(uuid == null) {
            Set<String> RegisteredEventTypes = jedis.smembers("Registered");
            int i = 0;
            for (String id : RegisteredEventTypes) {
                TypeMetaData[i] = eventTypeMetaData(jedis,id);
                i++;
            }
        }
        else{
            TypeMetaData[0] = eventTypeMetaData(jedis,uuid);
        }
        jedis.close();
        return TypeMetaData;
    }

    private static HashMap eventTypeMetaData(Jedis j,String uuid){
        HashMap<String,String> metaData = new HashMap<>();
        metaData.put("uuid",uuid);
        metaData.put("Name",j.get(uuid+":Name"));
        metaData.put("Definition",j.get(uuid+":Definition"));
        metaData.put("AvroShchema",j.get(uuid+":AvroSchema"));
        metaData.put("Inputs", j.smembers(uuid+":Inputs").toString());

        return metaData;
    }

    static Map registerWebHooks(String uuid,String url,String host) {
        Jedis jedis = new Jedis(host);
        long result = jedis.sadd(uuid+":WebHooks",url);
        jedis.close();
        if (result == 1) return U.map("response", "Web Hook "+url+" registered in "+uuid);
        else return U.map("result","Failed to register, try again");
    }

    static Map getWebHooks(String host){
        HashMap<String,String[]> allWebHooks= new HashMap<>();
        Jedis jedis = new Jedis(host);
        Set<String> RegisteredEventTypes = jedis.smembers("Registered");
        for(String uuid : RegisteredEventTypes){
            Set<String> uuidWebHooks = jedis.smembers(uuid+":WebHooks");
            String[] WebHooksArray = uuidWebHooks.toArray(new String[uuidWebHooks.size()]);
            allWebHooks.put(uuid, WebHooksArray);

        }
        jedis.close();
        return allWebHooks;
    }

    static Map deleteWebHook(String uuid, String url, String host) {
        Jedis jedis = new Jedis(host);
        LocalPersistenceMappingHelper redisComposerMapping = new LocalPersistenceMappingHelper(host);

        long result = jedis.srem(uuid+":WebHooks",url);
        String[] inputs = jedis.smembers(uuid + ":Inputs").toArray(new String[0]);
        for(int i = 0; i < inputs.length; i++) {
            /*counting--*/
            redisComposerMapping.decrEventCount(inputs[i]);
            /*remove from members*/
            jedis.srem(uuid+"Inputs",inputs[i]);
        }
        jedis.close();
        redisComposerMapping.close();

        if (result == 1)return U.map("response","web hook "+url+" deleted from "+uuid);
        else return U.map("result","Failed to delete, try again");
    }

}
