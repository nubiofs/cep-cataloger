
import com.fasterxml.uuid.Generators;

import junit.framework.TestCase;
import redis.clients.jedis.Jedis;


import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


public class CatalogerTest extends TestCase {

    public void testRegisterWebhook() throws IOException {

        redis.embedded.RedisServer redisServer = new redis.embedded.RedisServer(6379);

        redisServer.start();

        UUID uuid = Generators.timeBasedGenerator().generate();
        String url = "www.google.com";
        Map result = Cataloger.registerWebHooks(uuid.toString(),url,"localhost");

        Set<String> urlStored = new Jedis("localhost").smembers(uuid.toString()+":WebHooks");
        redisServer.stop();

        assertEquals(result.get("response"),"Web Hook "+url+" registered in "+uuid.toString());
        assertTrue(urlStored.contains(url));
    }

    public void testGetWebHooks() throws IOException {
        redis.embedded.RedisServer redisServer = new redis.embedded.RedisServer(6379);
        redisServer.start();

        Jedis j = new Jedis("localhost");

        UUID uuid = Generators.timeBasedGenerator().generate();
        String url = "www.google.com";
        j.sadd(uuid.toString()+":WebHooks",url);
        j.sadd("Registered",uuid.toString());

        String url2 = "www.facebook.com";
        j.sadd(uuid.toString()+":WebHooks",url2);

        UUID uuid3 = Generators.timeBasedGenerator().generate();
        String url3 = "www.google.com";
        j.sadd(uuid3.toString()+":WebHooks",url3);
        j.sadd("Registered",uuid3.toString());
        j.close();

        Map <String, String[]> result = Cataloger.getWebHooks("localhost");

        redisServer.stop();

        assertEquals("www.google.com",result.get(uuid3.toString())[0]);

    }

    public void testDeleteWebhook() throws IOException {

        redis.embedded.RedisServer redisServer = new redis.embedded.RedisServer(6379);

        redisServer.start();

        Jedis j = new Jedis("localhost");
        UUID uuid = Generators.timeBasedGenerator().generate();
        String url = "www.google.com";
        j.sadd(uuid.toString()+":WebHooks",url);
        j.sadd("Registered",uuid.toString());

        String url2 = "www.facebook.com";
        j.sadd(uuid.toString()+":WebHooks",url2);

        Map <String,String> result = Cataloger.deleteWebHook(uuid.toString(),url,"localhost");

        assertEquals("web hook "+url+" deleted from "+uuid.toString(),result.get("response"));
        assertFalse(j.sismember(uuid.toString()+":WebHooks",url));

        j.close();

        redisServer.stop();

    }

    public void testGetEventTypes() throws IOException {
        redis.embedded.RedisServer redisServer = new redis.embedded.RedisServer(6379);

        redisServer.start();

        Jedis j = new Jedis("localhost");
        UUID uuid = Generators.timeBasedGenerator().generate();
        String name = "Location";
        String definition = "SELECT * FROM Position";
        String AvroSchema  = "{..{{..{}...}..}..}..}";
        String [] Inputs = new String[2];
        Inputs[0] = "566";
        Inputs[1] = "0446";

        Set<String> inputs = new HashSet<String>();
        inputs.add(Inputs[1]);
        inputs.add(Inputs[0]);

        j.sadd("Registered",uuid.toString());

        j.set(uuid.toString()+":Name",name);
        j.set(uuid.toString()+":Definition",definition);
        j.set(uuid.toString()+":AvroSchema",AvroSchema);
        j.sadd(uuid.toString()+":Inputs",Inputs[0]);
        j.sadd(uuid.toString()+":Inputs",Inputs[1]);
        j.close();


        Map result = Cataloger.getTypes(uuid.toString(),"localhost");

        Map<String,String>[] response = (Map[]) result.get("response");



        assertFalse(result.isEmpty());
        assertEquals(uuid.toString(),response[0].get("uuid"));
        assertEquals(definition,response[0].get("Definition"));
        assertEquals(name,response[0].get("Name"));


        redisServer.stop();


    }

}
